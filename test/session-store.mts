/**
 * SPDX-PackageName: kwaeri/session-store
 * SPDX-PackageVersion: 1.1.1
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import * as assert from 'assert';
import { BaseSessionStore } from '../src/session-store.mjs';


// DEFINES
let configuration = {
        version: "0.1.0",
        type: "in-memory",
        store: {}
    },
    store = new BaseSessionStore( configuration ),
    sessionParams = {
        id: "0123456789",
        uk: "",
        host: "",
        domain: "",
        path: "",
        expires: 1585411327634,
        persistent: true,
        user: {
            username: "",
            email: "guest@example.com",
            name: {
                first: "Test",
                last: "User"
            }
        },
        authenticated: false
    },
    newSessionParams = {
        id: "1123456789",
        uk: "",
        host: "",
        domain: "",
        path: "",
        expires: new Date( new Date().getTime() + ( 30 * 60000 ) ).getTime(),
        persistent: true,
        user: {
            username: "",
            email: "guest@example.com",
            name: {
                first: "Test",
                last: "User"
            }
        },
        authenticated: false
    };


// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {

        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    async () => {
                        //const version = JSON.parse( ( await fs.readFile( path.join( './', 'package.json' ), { encoding: "utf8" } ) ) ).version;

                        //console.log( `VERSION: ${version}` );

                        return Promise.resolve(
                            assert.equal( [1,2,3,4].indexOf(4), 3 )
                        );
                    }
                );

            }
        );

    }
);


// Primary tests for the module
describe(
    'Session Store Functionality Test Suite',
    () => {


        describe(
            'Create Session Test',
            () => {

                it(
                    'Should return a newly stored session found by id.',
                    () => {

                        let returned = store.createSession( sessionParams.id, sessionParams );

                        assert.equal( JSON.stringify( sessionParams ), JSON.stringify( returned ) );
                    }
                );

            }
        );

        describe(
            'Get Session Test',
            () => {
                it(
                    'Should return a previously stored session found by id.',
                    () => {
                        let returned = store.getSession( sessionParams.id );

                        assert.equal( JSON.stringify( sessionParams ), JSON.stringify( returned ) );
                    }
                );
            }
        );

        describe(
            'Set Session Parameter Test',
            () => {
                it(
                    'Should return the value of a parameter set in a stored session found by id.',
                    () => {
                        let returned = store.set( sessionParams.id, 'authenticated', true );

                        assert.equal( true, returned );
                    }
                );
            }
        );

        describe(
            'Get Session Parameter Test',
            () => {
                it(
                    'Should return the value of a parameter previously set in a stored session found by id.',
                    () => {
                        let returned = store.get( sessionParams.id, 'authenticated', true );

                        assert.equal( true, returned );
                    }
                );
            }
        );

        describe(
            'Clean Sessions Test',
            () => {
                it(
                    'Should delete any session(s) which have an expires time stamp that is less than the current timestamp.',
                    () => {
                        const ts                  = new Date(),
                              created             = store.createSession( newSessionParams.id, newSessionParams ),
                              returned            = store.cleanSessions();

                        assert.equal( store.deleteSession( sessionParams.id ), false );
                    }
                );
            }
        );

        describe(
            'Delete Sessions Test',
            () => {
                it(
                    'Should delete the specified session from the session store.',
                    () => {
                        assert.equal( store.deleteSession( "1123456789" ), true );
                    }
                );
            }
        );


    }
);
