# [![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) kwaeri-node-kit-session-store [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

A Massively Modified Open Source Project by kirvedx

[![GPG/Keybase](https://img.shields.io/badge/GPG-1B842CB5%20Rik-inactive?style=for-the-badge&label=GnuPG2%2FKeybase&logo=gnu+privacy+guard&color=0093dd)](https://keybase.io/rik)
[![Google](https://img.shields.io/badge/Google%20Developers-kirvedx-inactive?style=for-the-badge&logo=google+tag+manager&color=414141)](https://developers.google.com/profile/u/117028112450485835638)
[![GitLab](https://img.shields.io/badge/GitLab-kirvedx-inactive?style=for-the-badge&logo=gitlab&color=fca121)](https://github.com/kirvedx)
[![GitHub](https://img.shields.io/badge/GitHub-kirvedx-inactive?style=for-the-badge&logo=github&color=181717)](https://github.com/kirvedx)
[![npm](https://img.shields.io/badge/NPM-Rik-inactive?style=for-the-badge&logo=npm&color=CB3837)](https://npmjs.com/~rik)

The kwaeri/session-store component of the kwaeri/node-kit application framework

[![pipeline status](https://gitlab.com/kwaeri/node-kit/session-store/badges/main/pipeline.svg)](https://gitlab.com/kwaeri/node-kit/session-store/commits/main)  [![coverage report](https://gitlab.com/kwaeri/node-kit/session-store/badges/main/coverage.svg)](https://kwaeri.gitlab.io/node-kit/session-store/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

## TOC
* [The Implementation](#the-implementation)
* [Getting Started](#getting-started)
  * [Installation](#installation)
  * [Usage](#usage)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

kwaeri/session-store reinvents and modernizes the session-store portion of the node-kit application framework - a major component of the kwaeri application platform.

As the session-store component was originally baked into the "nk" module, its usage was entirely controlled by it. As we discern the process for decoupling the individual components which make up a kwaeri/node-kit application, we'll begin to simplify the act of doing so, and provide documentation for utilizing each component (cumulatively and individually).

## Getting Started

**NOTE**

This module is not ready for production, but published for testing and development purposes. It is in a beta state that is intended for allowing a larger audience of users to try out anything that may already be available, but please be aware that there is likely many aspects of the platform which are not working and/or completely broken; This is intended to allow users to find and report such issues so that they may fixed. Updated documentation and complete examples and tutorials for getting started will be provided as the platform rewrite nears completion.

### Installation

[kwaeri/node-kit](https://www.npmjs.com/package/@kwaeri/node-kit) wraps the various components under the kwaeri scope and provides a single point of entry to both the kwaeri/node-kit application framework, and the kwaeri/cli components of the kwaeri platform, for easing the process of building a kwaeri application.

[kwaeri/cli](https://www.npmjs.com/package/@kwaeri/cli) wraps the various user-executable framework components under the kwaeri scope, and provides a single point of entry to the CLI tooling of the kwaeri platform.

If you wish to use kwaeri/session-store - perform the following steps:

Install kwaeri/session-store:

```bash
npm install @kwaeri/session-store
```

### Usage

This module implements the `SessionStore` interface, and `BaseSessionStore` class that all session stores and session providers implement, so that they meet the requirements for enabling session management as part of the node-kit application framework. It is made available so that derived stores and providers are able to inherit from it.

As documentation for deriving new session-stores and session-providers becomes available, this documentation will be updated to reference it. For now, it is recommended that if one wishes to write their own session-store and session provider, that they should peruse the various session-stores and session-providers that already exist;

* [In-Memory](#usage) (Offered by the `BaseSessionStore` implementation).
* [Filesystem](https://gitlab.com/kwaeri/node-kit/filesystem-session-store)
* [Database (MySQL)](https://gitlab.com/kwaeri/node-kit/database-session-store)
* [Memcached (Historically preferred)](https://gitlab.com/kwaeri/node-kit/memcached-session-store)
* [Redis (Recommended, coming soon)](https://gitlab.com/kwaeri/node-kit/redis-session-store)

Through these one should be able to identify how a new provider and store would need to be made so as to work properly with the node-kit application framework. If, for instance, you'd like to contribute the Redis provider - please contact me (see [how to contribute](#how-to-contribute-code) below) and I'll create the project or add you as a maintainer after you've made a proper merge-request with a functional - to-standard - implementation.

Please keep in mind that session-stores and session-providers (used by the node-kit application framework) are not the same thing as a service-provider (used by the user-executable framework), and that the two do not share the same interface, implementation patterns, etc.

To leverage the session-store interface in deriving a new session-store, you'll first need to include it in your derived session-store:

```javascript
// INCLUDES
import { BaseSessionStore } from '@kwaeri/session-store';
```

If you need to recreate the `BaseSessionStore` for your store and provider, make sure you at least implement the `SessionStore` interface as it exists, and ensure that your new derivation works properly through that interface; It is absolutely required for sessions to work properly with the node-kit application framework.

**NOTE**

As mentioned earlier, the plan is to continue development of the individual components of the node-kit application framework - the session-store component included - and ultimately ease the process of making use of each individual component as they are decoupled from one another (from their linear, monolithic, ancestor).

More documentation to come!

## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

The project also leverages Keybase for communication and alerts - outside of standard email. To join our keybase chat, run the following from terminal (assuming you have [keybase](https://www.keybase.io) installed and running):

```bash
keybase team request-access kwaeri
```

Alternatively, you could search for the team in the GUI application and request access from there.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/kwaeri-node-kit/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:incoming+mmod/kwaeri-node-kit@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-node-kit/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
