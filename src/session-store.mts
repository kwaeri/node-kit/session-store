/**
 * SPDX-PackageName: kwaeri/session-store
 * SPDX-PackageVersion: 1.1.1
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import debug from 'debug';
import { kdt } from '@kwaeri/developer-tools';


// DEFINES
const _ = new kdt(),
      DEBUG = debug( 'nodekit:base-session-store' );


export type SessionBits = {
    id: string,
    uk: string,
    host: string,
    domain: string,
    path: string,
    persistent: boolean;
    expires: Date|number,
    authenticated: boolean,
    user?: SessionUserBits
};

export type SessionUserBits = {
    username: string,
    email: string,
    name: {
        first: string,
        last?: string
    }
};

export type SessionCache = {
    [key: string]: SessionBits;
};

export class ClientSession {
    public id: string;
    //public get id(): string { return this._id; }
    //public set id( value: string ) { this._id = value; }

    public uk: string;
    //public get uk(): string { return this._uk; }
    //public set uk( value: string ) { this._uk = value; }

    public host: string;
    //public get host(): string { return this._host; }
    //public set host( value: string ) { this._host = value; }

    public domain: string;
    //public get domain(): string { return this._domain; }
    //public set domain( value: string ) { this._domain = value; }

    public path: string;
    //public get path(): string { return this._path; }
    //public set path( value: string ) { this._path = value; }

    public persistent: boolean;
    //public get persistent(): boolean { return this._persistent; }
    //public set persistent( value: boolean ) { this._persistent = value; }

    public expires: Date|number;
    //public get expires(): Date|number { return this._expires; }
    //public set expires( value: Date|number ) { this._expires = value; }

    public authenticated: boolean;
    //public get authenticated(): boolean { return this._authenticated; }
    //public set authenticated( value: boolean ) { this._authenticated = value; }

    public user: SessionUserBits;
    //public get user(): SessionUserBits { return this._user; }
    //public set user( value: SessionUserBits ) { this._user = value; }

    constructor( seed: SessionBits ) {
        this.id             = seed.id;
        this.uk             = seed.uk;
        this.host           = seed.host;
        if( seed.domain )
            this.domain = seed.domain;
        else
            this.domain = "";
        this.path           = seed.path || '/';
        this.persistent     = seed.persistent || true;
        this.expires        = seed.expires;
        this.authenticated  = seed.authenticated || false;
        this.user           = seed.user!;
    }
}


/**
 * The SessionStore interface
 */
export interface SessionStore {
    /**
     * Creates a new client session
     *
     * @param { string } id The string id of the client session
     * @param { SessionBits } clientSession The bits of the client session
     *
     * @returns { ClientSession } an instance of a {@link ClientSession}
     */
    createSession( id: string, sessionBits: SessionBits ): ClientSession|Promise<ClientSession>;

    /**
     * Retrieves a client session from the session cache
     *
     * @param { string } id The string id of the client session requested
     *
     * @returns { ClientSession } an instance of a {@link ClientSession}
     */
    getSession( id: string ): ClientSession|Promise<ClientSession|null>|null;

    /**
     * Deletes a client session
     *
     * @param { string } id The string id of the client session to be deleted
     *
     * @returns { boolean } True if successful, false otherwise
     */
    deleteSession( id: string ): boolean|Promise<boolean>;

    /**
     * Validates sessions, removing expired sessions.
     *
     * @param { void }
     *
     * @returns { void }
     */
    cleanSessions(): void|Promise<void>;

    /**
     * Get a value from a client session
     *
     * @param { string } id The string id of the client session from which to retrieve a value
     * @param { string } name The string name of the value to retrieve from the client session
     * @param { any } defaultValue A value to assume if no session - or value - is found
     *
     * @returns { any|null } The value requested, the default, or null
     */
    get( id: string, name: string, defaultValue: any ): any|null|Promise<any|null>;

    /**
     * Set a value on a client session
     *
     * @param { string } id
     * @param { string } name
     * @param { any } value
     *
     * @returns { any } The value set (if session was found) or null
     */
    set( id: string, name: string, value: any ): any|null|Promise<any|null>;
}


/**
 * In-Memory Session Store
 *
 * Manages a 'store' object, where each property is a session named after its 'ID'.
 * For each ID property, is another object that is the session, and all of its
 * associated information.
 */
export class BaseSessionStore implements SessionStore {
    /**
     * @var { string }
     */
    public version: string;

    /**
     * @var { string }
     */
    public type: string;

    /**
     * @var { any }
     */
    protected configuration: any;

    /**
     * @var { Object } store
     */
    private store: SessionCache;

    constructor( configuration: any ) {
        this.store = _.get( configuration, 'store', {} );

        // Organize all of the uncertainty:
        this.version     = ( configuration && configuration.version ) ? configuration.version : null,
        this.type        = ( configuration && configuration.type ) ? configuration.type : "In-Memory";

        this.configuration = configuration;
    }

    public createSession( id: string, sessionBits: SessionBits = {} as SessionBits ): ClientSession {
        DEBUG( `Create session '%s'`, id );

        this.store = _.set( this.store, id, sessionBits );

        return _.get( this.store, id, null );
    }

    public getSession( id: string ): ClientSession|null {
        DEBUG( `Get session '%s'`, id );

        return _.get( this.store, id, null );
        //return _.has( this.store, id ) ? this.store[id] : null;
        //return ( ( this.store.hasOwnProperty( id ) ) ? this.store[id] : null );
    };

    public deleteSession( id: string ): boolean {
        if( _.has( this.store, id ) ) {
            DEBUG( `Delete session '${id}'` );

            delete this.store[id];

            return true;
        }

        DEBUG( `Session '${id}' was not deleted because it could not be found` );

        return false;
    }

    public cleanSessions(): void {
        let stamp,
            ts = new Date(),
            count = 0;

        if( this.store ) {
            DEBUG( `Cleaning sessions` );

            for( const session in this.store ) {
                stamp = new Date( this.store[session].expires );
                if( ts > stamp  ) {
                    DEBUG( `Clean session '${this.store[session].id}'` );

                    this.deleteSession( session );
                }

                count++;
            }
        }
    }

    public get( id: string, name: string, defaultValue?: any ): any|null {
        DEBUG( `Get '%s' from '%s' [%s] `, name, id, _.get( this.store, `${id}.${name}`, defaultValue ) );

        //if( this.store.hasOwnProperty( id ) )
        if( _.has( this.store, id ) )
            return _.get( this.store, `${id}.${name}`, defaultValue );

        DEBUG( `Error getting '%s' from '%s': session not found`, name, id );

        return null;
    }

    public set( id: string, name: string, value: any ): any|null {
        console.log( typeof this.store[id] );

        if( _.has( this.store, id ) && _.type( this.store[id as any] ) === "object" ) {
            DEBUG( `Set '%s' to '%s' for '%s'`, name, value, id );

            ( this.store[id] as any )[name] = value;

            return ( this.store[id] as any )[name];
        }

        DEBUG( `Error setting '%s' to '%s' for '%s': session not found`, name, value, id );

        return null;
    }
}